"""
Definir una función que cargue una lista con palabras y la retorne.
Luego otra función tiene que mostrar todas las palabras de la lista que tienen más de 5 caracteres.
"""

def cargaPalabras():
    palabras=[]
    for i in range(5):
        palabra=input("Ingrese palabra por favor.  ")
        palabras.append(palabra)
    return palabras

def masDeCinco(lista):
    mayor=""
    for p in lista:
        if p>mayor:
            mayor=p
    print("la palabra mayor es ", mayor)

palabras=cargaPalabras()
masDeCinco(palabras)