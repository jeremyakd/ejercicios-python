# A) - Imprima los dos primero caracteres.

""" def dosPrimeros(cadena):
    x=0
    while x<2:
        print(cadena[x])
        x+=1

dosPrimeros("cadena") """

# B) - Imprima los tres ultimos caracteres.

""" def tresUltimos(cadena):
    l=len(cadena)
    #print(l)
    for i in range(l+1):
        if i==(l-2):
            print(cadena[i-1])
        elif i==(l-1):
            print(cadena[i-1])
        elif i==l:
            print(cadena[i-1])

tresUltimos("cadena") """

# C) - Imprima dicha cadena cada 2 caracteres.

""" def cadaDos(cadena):
    c=cadena
    l=len(cadena)
    for i in range(0,l,2):
        print(c[i])

cadaDos("Desarrollando en Python") """

# D) - Dicha cadena en sentido inverso.

""" def inverso(cadena):
    c=cadena
    l=len(cadena)
    while l>0:
        print(c[l-1])
        l-=1

inverso("cadena") """

# E) - Imprima la cadena en un sentido y en sentido inverso.

def dobleSentido(cadena):
    cadena1=cadena
    cadena2=""
    l=len(cadena)
    while l>0:
        cadena2+=cadena1[l-1]
        l-=1
    print(cadena1+cadena2)


dobleSentido("cadena")