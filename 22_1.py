"""
Crear dos listas paralelas. En la primera ingresar los nombres de empleados y en la segunda los sueldos de cada empleado.
Ingresar por teclado cuando inicia el programa la cantidad de empleados de la empresa.
Borrar luego todos los empleados que tienen un sueldo mayor a 10000 (tanto el sueldo como su nombre)
"""

empleados=[]
sueldo=[]

x=int(input("Cuantos empleados ingresara. "))

for i in range(x):
    emp=input("Ingrese nombre por favor. ")
    empleados.append(emp)
    sue=int(input("Ingrese sueldo por favor. "))
    sueldo.append(sue)
pos=0

while pos<len(empleados):
    if sueldo[pos]>10000:
        empleados.pop(pos)
        sueldo.pop(pos)
    else:
        pos+=1

print(empleados)
print(sueldo)