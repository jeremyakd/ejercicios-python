class Jugador:
    def __init__(self,nombre,puntaje):
        self.nombre=nombre
        self.puntaje=puntaje

    def __str__(self):
        if int(self.puntaje)<1000:
            cadena="El jugador " + self.nombre + " es principiante. "
        else:
            cadena="El jugador " + self.nombre + " es experto."
        return cadena


jugador1=Jugador("Jeremias", 1100)
jugador2=Jugador("Daniel", 1000)
jugador3=Jugador("pedro", 800)
jugador4=Jugador("juan", 100)
jugador5=Jugador("kevin", 1300)

print(jugador1)
print(jugador2)
print(jugador3)
print(jugador4)
print(jugador5)