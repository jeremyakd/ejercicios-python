class Triangulo:

    def inicializar(self):
        self.lado1=int(input("Ingrese primer lado. "))
        self.lado2=int(input("Ingrese segundo lado. "))
        self.lado3=int(input("Ingrese tercer lado. "))

    def imprime(self):
        print("--------------------------")
        print("Lado 1: ", self.lado1)
        print("Lado 2: ", self.lado2)
        print("Lado 3: ", self.lado3)
            
    
    def imprimeMayor(self):
        print("--------------------------")
        print("Lado mayor: ")
        if self.lado1>self.lado2 and self.lado1>self.lado3:
            print("Lado uno")
        else:
            if self.lado2<self.lado3:
                print("Lado dos")
            else:
                print("Lado tres")
        

    def esEquilatero(self):
        print("--------------------------")
        if self.lado1==self.lado2 and self.lado1==self.lado3:
            print("Es equilatero")
        else: 
            print("No es equilatero")


tri=Triangulo()
tri.inicializar()
tri.imprime()
tri.imprimeMayor()
tri.esEquilatero()