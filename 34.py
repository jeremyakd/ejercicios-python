"""
Crear un diccionario en Python que defina como clave el número de documento de una persona 
y como valor un string con su nombre.

Desarrollar las siguientes funciones:
1) Cargar por teclado los datos de 4 personas.
2) Listado completo del diccionario.
3) Consulta del nombre de una persona ingresando su número de documento.
"""

def cargaPersonas():
    personas={}
    for i in range(5):
        dni=int(input("Ingrese DNI: "))
        nom=input("Ingrese Nombre: ")
        personas[dni]=nom
    return personas

def imprime(diccionario):
    for dni in diccionario:
        print("DNI: ", dni, " Nombre: ",diccionario[dni])

def consulta(dic):
    doc=int(input("Ingrese DNI:"))
    if doc in dic:
        print("DNI: ", doc, " Nombre: ",dic[doc])

personas=cargaPersonas()
imprime(personas)
consulta(personas)