"""
Ingresar una oración que pueden tener letras tanto en mayúsculas como minúsculas.
Contar la cantidad de vocales.
Crear un segundo string con toda la oración en minúsculas para que sea más fácil 
disponer la condición que verifica que es una vocal.
"""

cadena="tan bella es la Luna a alguien como tu no la cambio por ninguna"
cadenalow=cadena.lower()
vocal=0
x=0

while x<len(cadenalow):
    if cadenalow[x]=="a" or cadenalow[x]=="e" or cadenalow[x]=="i" or cadenalow[x]=="o" or cadenalow[x]=="u":
        vocal+=1
    x+=1
print(str(vocal))