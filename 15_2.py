"""
Cargar por teclado y almacenar en una lista las alturas de 5 personas (valores float)
Obtener el promedio de las mismas. 
Contar cuántas personas son más altas que el promedio y cuántas más bajas.
"""

alturas=[]
sum=0
x=0
mayProm=0
menProm=0
for i in range(5):
    alt=float(input("Ingrese Altura por favor"))
    alturas.append(alt)
    sum+=alt
promedio=sum/5
#print(str(sum))
#print(str(promedio))

while x<5:
    if alturas[x]>promedio:
        mayProm+=1
    else:
        menProm+=1
    x+=1

print("Cantidad mayor al promedio = " + str(mayProm))
print("Cantidad menor al promedio = " + str(menProm))