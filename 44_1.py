class Cuadrado:
    def __init__(self, lado):
        self.lado=lado

    def perimetro(self):
        perimetro=self.lado*4
        print("El perimetro es: ", perimetro)

    def superficie(self):
        superficie=self.lado*self.lado
        print("La superficie es: ", superficie)

cuadrado=Cuadrado(3)
cuadrado.perimetro()
cuadrado.superficie()
