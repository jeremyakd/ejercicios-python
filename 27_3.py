"""
Confeccionar un programa que permita:
1) Cargar una lista de 10 elementos enteros.
2) Generar dos listas a partir de la primera. 
En una guardar los valores positivos y en otra los negativos.
3) Imprimir las dos listas generadas.
"""


def cargaDatos():
    nums=[]
    for i in range(10):
        num=int(input("Ingrese numero por favor. "))
        nums.append(num)
    return nums

def generaLista(lista):
    pos=[]
    neg=[]
    for i in range(len(lista)):
        if lista[i]>0:
            pos.append(lista[i])
        else:
            neg.append(lista[i])
    return pos, neg

def imprime(lista):
    for i in range(len(lista)):
        print(str(lista[i]))

li=cargaDatos()
posi, nega=generaLista(li)
imprime(posi)
imprime(nega)


#print(pos)
#print(neg)