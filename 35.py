"""
Se desea almacenar los datos de 3 alumnos. 
Definir un diccionario cuya clave sea el número de documento del alumno. 
Como valor almacenar una lista con componentes de tipo tupla donde almacenamos nombre de materia y su nota.

Crear las siguientes funciones:

1) Carga de los alumnos (de cada alumno solicitar su dni y los nombres de las materias y sus notas)
2) Listado de todos los alumnos con sus notas
3) Consulta de un alumno por su dni, mostrar las materias que cursa y sus notas.
"""

def cargaAlumo():
    alumnos={}
    continuar1="s"
    while continuar1=="s":
        dni=int(input("Ingrese DNI: "))
        continuar2="s"
        lista=[]
        while continuar2=="s":
            mat=input("Ingrese nombre de la materia")
            nota=int(input("Ingrese nota por favor"))
            lista.append((mat, nota))
            continuar2=input("Desea seguir cargando... ( s / n ) ")
        alumnos[dni]=lista
        continuar1=input("Desea cargar otro alumno ... ( s / n ) ")
    return alumnos

def imprimeAlu(listaDeAlumnos):
    for dni in listaDeAlumnos:
        print("AlumnoDNI: ", dni)
        for m, n in listaDeAlumnos[dni]:
            print("Materia: ", m , " Nota: ", n)

def consulta(listaDeAlumnos):
    doc=int(input("Ingrese DNI para consultar: "))
    for doc in listaDeAlumnos:
        print("Alumno DNI: ", doc)
        for m,n in listaDeAlumnos[doc]:
            print(m, n)

alumnos=cargaAlumo()
imprimeAlu(alumnos)
consulta(alumnos)
    

        