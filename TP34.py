# A) Inserte el caracter entre cada letra de la cadena.

""" def insertaCaracter(cadena, caracter):
    cadena2=""
    f=0
    for i in cadena:
        cadena2+=i
        f+=1
        if f<len(cadena):
            cadena2+=caracter
    print(cadena2)

insertaCaracter("cadena", "-") """

# B) Reemplace todo los espacios por el caracter.

""" def reemplazaPorCaracter(cadena, caracter):
    cadena2=""
    for i in range(len(cadena)):
        if cadena[i]==" ":
            cadena2+=caracter
        else:
            cadena2+=cadena[i]
    print(cadena2)

reemplazaPorCaracter("Racing Club", "-") """

# C) - Reemplace todos los digitos en la cadena por el caracter.

""" def remplazaDigito(cadena, carcter):
    cadena2=""
    for i in cadena:
        if (i.isdigit()):
            cadena2+=carcter
        else:
            cadena2+=i
    return cadena2

print(remplazaDigito("En el ano 2019 ganamos nuestro titulo numero 18", "*" )) """


# D) - Inserte el caracter cada 3 digitos en la cadena

def inserteCaracterEnCadena(cadena, caracter):
    l=len(cadena)
    cadena2=""
    for i in range(l):
        if i>0:
            if i%3==0:
                cadena2+=caracter
        cadena2+=cadena[i]
        
    print(cadena2)

inserteCaracterEnCadena("2552552550",".")
