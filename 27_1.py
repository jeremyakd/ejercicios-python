"""
En una empresa se almacenaron los sueldos de 10 personas.
Desarrollar las siguientes funciones y llamarlas desde el bloque principal:
1) Carga de los sueldos en una lista.
2) Impresión de todos los sueldos.
3) Cuántos tienen un sueldo superior a $4000.
3) Retornar el promedio de los sueldos.
4) Mostrar todos los sueldos que están por debajo del promedio.
"""

def cargaSueldo():
    sueldos=[]
    for i in range(10):
        sue=int(input("Ingrese sueldo por favor. "))
        sueldos.append(sue)
    return sueldos

def imprimeSueldos(lista):
    for i in range(len(lista)):
        print(str(lista[i]))

def mayorA(lista):
    aux=0
    for i in range(len(lista)):
        if lista[i]>4000:
            aux+=1
    return aux

def promedio(lista):
    sum=0
    for i in range(len(lista)):
        sum+=lista[i]
    prom=sum//10
    return prom

def porDebajo(lista, pro):
    print("Lista de sueldos menores al promedio: \n")
    for i in range(len(lista)):
        if lista[i]<pro:
            print(str(lista[i]))

sueldos=cargaSueldo()
imprimeSueldos(sueldos)
print("Los sueldos mayores a 4000 son: ",mayorA(sueldos))
prom=promedio(sueldos)
porDebajo(sueldos,prom)