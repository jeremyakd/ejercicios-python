"""
Confeccionar una función que reciba tres enteros y los muestre ordenados de menor a mayor.
En otra función solicitar la carga de 3 enteros por teclado
y proceder a llamar a la primer función definida.
"""

def orden(v1,v2,v3):
    if v1<v2 and v1<v3:
        if v2<v3:
            print(v1)
            print(v2)
            print(v3)
        else:
            print(v1)
            print(v3) 
            print(v2)
    else:
        if v2<v3:
            if v1<v3:
                print(v2)
                print(v1)
                print(v3)
            else:
                print(v2)
                print(v3)
                print(v1)
        else:
            if v1<v2:
                print(v3)
                print(v1)
                print(v2)
            else:
                print(v3)
                print(v2)
                print(v1)

def num():
    val1=int(input("Ingrese Valor"))
    val2=int(input("Ingrese Valor"))
    val3=int(input("Ingrese Valor"))
    orden(val1, val2, val3)

num()