class Jugador:
    minutos=30

    def __init__(self, nombre, puntaje):
        self.nombre=nombre
        self.puntaje=puntaje

    def imprimir(self):
        print("Nombre: ", self.nombre)
        print("Puntaje: ", self.puntaje)
        print("Termina el partido en: ", Jugador.minutos, " minutos")


    def pasarTiempo(self):
        Jugador.minutos-=1

jugador1=Jugador("jerem", 100)
jugador2=Jugador("kevin", 150)

while Jugador.minutos>0:
    jugador1.imprimir()
    jugador2.imprimir()
    jugador1.pasarTiempo()
