"""
Realizar un programa que solicite la carga de valores enteros por teclado y los sume.
Finalizar la carga al ingresar el valor -1. 
Dejar como comentario dentro del código fuente el enunciado del problema.
"""

suma=0
j=int(input("ingrese numero (-1 finaliza)")) #primera carga
while j!=-1:
    suma+=j
    j=int(input("ingrese numero (-1 finaliza)")) #resto de cargas
print("La Suma de todos los valores = " + str(suma))
