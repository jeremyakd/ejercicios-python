"""
Definir una lista de enteros por asignación en el bloque principal. 
Llamar a una función que reciba la lista y nos retorne el producto de todos sus elementos. 
Mostrar dicho producto en el bloque principal de nuestro programa.
"""

def producto(lista):
    aux=lista[0]
    for i in range(1,len(lista)):
        aux*=lista[i]
    return aux

lista=[2,2,2,2,2,2]
print(producto(lista))