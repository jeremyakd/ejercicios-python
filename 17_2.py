"""
En un curso de 4 alumnos se registraron las notas de sus exámenes y se deben procesar de acuerdo a lo siguiente:
a) Ingresar nombre y nota de cada alumno (almacenar los datos en dos listas paralelas)
b) Realizar un listado que muestre los nombres, notas y condición del alumno. En la condición,
colocar "Muy Bueno" si la nota es mayor o igual a 8, "Bueno" si la nota está entre 4 y 7, y colocar "Insuficiente" si la nota es inferior a 4.
c) Imprimir cuantos alumnos tienen la leyenda “Muy Bueno”.
"""

alumnos=[]
notaAlumnos=[]
for i in range(4):
    alu=input("Ingrese nombre de alumno. ")
    alumnos.append(alu)
    nota=float(input("Ingrese nota. "))
    notaAlumnos.append(nota)

muyBueno=0

for i in range(4):
    print(alumnos[i])
    print(notaAlumnos[i])
    if notaAlumnos[i]>=8:
        muyBueno+=1
        print("Muy Bueno")
    elif notaAlumnos[i]>=4 and notaAlumnos[i]<=7:
        print("Bueno")
    else:
        print("Insuficiente")

print("Cantidad de alumnos 'Muy Bueno': " + str(muyBueno))