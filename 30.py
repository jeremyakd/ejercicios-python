"""
Confeccionar una función que reciba una serie de edades
 y me retorne la cantidad que son mayores o iguales a 18 
(como mínimo se envía un entero a la función)
"""

def mayores(num1,*li):
    k=0
    if num1>=18:
        k+=1
    for i in range(len(li)):
        if li[i]>=18:
            k+=1
    return k

print(mayores(19,29,3,49,5))