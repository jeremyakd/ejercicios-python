""" 
Solicitar el ingreso de una clave por teclado y almacenarla en una cadena de caracteres.
Controlar que el string ingresado tenga entre 10 y 20 caracteres para que sea válido, 
en caso contrario mostrar un mensaje de error. 
"""

cadena=input("Ingrese clave por favor")
x=0
while len(cadena)<10 or len(cadena)>20:
    print("Error")
    cadena=input("Ingrese clave por favor")

print("Gracias")