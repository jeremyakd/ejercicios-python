"""
 Confeccionar una función que calcule la superficie de un rectángulo y la retorne, 
 la función recibe como parámetros los valores de dos de sus lados:

 def retornar_superficie(lado1,lado2):

En el bloque principal del programa cargar los lados de dos rectángulos y 
luego mostrar cual de los dos tiene una superficie mayor.
 """

def retornar_superficie(lado1, lado2):
    sup=lado1*lado2
    return sup


lado1=int(input("Ingrese lado por favor. "))
lado2=int(input("Ingrese lado por favor. "))
super1=retornar_superficie(lado1, lado2)
lado3=int(input("Ingrese lado por favor. "))
lado4=int(input("Ingrese lado por favor. "))
super2=retornar_superficie(lado3, lado4)

if super1==super2:
    print("Las superficies son iguales")
elif super1>super2:
    print( "El primer rectangulo tiene mayor sup.")
else:
    print("El segundo rectangulo tiene mayor sup.")


#print("Superficie del primer cuadrado: ", super1, " M2")
#print("Superficie del primer cuadrado: ", super2, " M2")