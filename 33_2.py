"""
            Almacenar los nombres de 5 productos y sus precios. 
            Utilizar una lista y cada elemento una tupla con el nombre y el precio.
            Desarrollar las funciones:

            1) Cargar por teclado.
            2) Listar los productos y precios.
            3) Imprimir los productos con precios comprendidos entre 10 y 15.
"""

def cargaProductos():
    productos=[]
    for i in range(5):
        nom=input("Ingrese producto por favor. ")
        precio=int(input("Ingrese precio por favor. "))
        productos.append((nom,precio))
    return productos

def imprime(lista):
    for p,pr in lista:
        print("Producto: ", p, "  Precio: " , pr)

def entre(lista):
    print("Productos con precio entre 10-15")
    for p,pr in lista:
        if pr<15 and pr>10:
            print(p)


productos=cargaProductos()
imprime(productos)
entre(productos)