"""
Confeccionar un programa con las siguientes funciones:

1)Cargar el nombre de un empleado y su sueldo. Retornar una tupla con dichos valores

2)Una función que reciba como parámetro dos tuplas con los nombres y sueldos de empleados
y muestre el nombre del empleado con sueldo mayor.
En el bloque principal del programa llamar dos veces a la función de carga
y seguidamente llamar a la función que muestra el nombre de empleado con sueldo mayor.
# bloque principal

empleado1=cargar_empleado()
empleado2=cargar_empleado()
mayor_sueldo(empleado1,empleado2)
"""

def cargaEmpleado():
    nom=input("Ingrese Nombre por favor. ")
    sue=int(input("Ingrese sueldo por favor. "))
    return (nom,sue)

def muestraMayor(t1, t2):
    if t1[1]>t2[1]:
        print(t1[0])
    else:
        print(t2[0])

tup1=cargaEmpleado()
tup2=cargaEmpleado()
muestraMayor(tup1, tup2)
