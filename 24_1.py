"""
Desarrollar una funcion que reciba un string como parametro y nos muestre la cantidad de vocales. 
Llamarla desde el bloque principal del programa 3 veces con string distintos.
"""

def vocales(cadena):
    vocal=0
    for i in cadena:
        if i=="a" or i=="e" or i=="i" or i=="o" or i=="u":
            vocal+=1
    print("La cadena tiene: " + str(vocal) + " Vocales. ")

vocales("anaconda")
vocales("murcielago")
vocales("hay")