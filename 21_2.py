"""
Definir una lista y almacenar los nombres de 3 empleados.

Por otro lado definir otra lista y almacenar en cada elemento una 
sublista con los números de días del mes que el empleado faltó.

Imprimir los nombres de empleados y los días que faltó.

Mostrar los empleados con la cantidad de inasistencias.

Finalmente mostrar el nombre o los nombres de empleados que faltaron menos días.

"""

empleados=[]
faltas=[]

for i in range(3):

    nom=input("Ingrese Nombre por favor. ")
    empleados.append(nom)
    dias=int(input("Cuantos dia falto el empleado " + nom + " "))
    faltas.append([])

    for j in range(dias):
        dia=int(input("Ingrese dia por favor"))
        faltas[i].append(dia)
    
        
print(empleados)
print(faltas)

for i in range(3):
    print("El Empleado: " + empleados[i] + " Falto los dias ", faltas[i])

men=len(faltas[i])

for i in range(1,3):
    if len(faltas[i])<men:
        men=len(faltas[i])

print("El empleado que menos falto es: ", empleados[men])