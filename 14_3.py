"""
Definir una lista que almacene por asignación los nombres de 5 personas. 
Contar cuantos de esos nombres tienen 5 o más caracteres.
"""

lista=["jeremias", "gaston", "alexis", "victor", "mario"]
#print(str(len(lista[0])))
may=0
x=0

while x<len(lista):
    f=len(lista[x])
    #print(lista[x])
    #print(str(f))
    if f>=5:
        may+=1
        #print(str(may) + " vuelta, Agregado " + lista[x])
    x+=1

print("Nombres con 5 caracteres o mas: " + str(may))
