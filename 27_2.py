"""
Desarrollar una aplicación que permita ingresar por teclado los nombres de 5 artículos y sus precios.
Definir las siguientes funciones:
1) Cargar los nombres de articulos y sus precios.
2) Imprimir los nombres y precios.
3) Imprimir el nombre de artículo con un precio mayor
4) Ingresar por teclado un importe y 
luego mostrar todos los artículos con un precio menor igual al valor ingresado.
"""
articulos=[]
precios=[]


def cargarDatos():
    for i in range(5):
        art=input("Ingrese nombre de articulo por favor. ")
        articulos.append(art)
        pre=int(input("Ingrese precio por favor. "))
        precios.append(pre)

def imprimePrecios(lista, lista2):
    for i in range(5):
        print("Articulo: ", lista[i], "Precio: ", str(lista2[i]))

def mayorPrecio(lista, lista2):
    aux=lista2[0]
    j=0
    for i in range(1,5):
        if lista2[i]>aux:
            aux=lista2[i]
            j=i
    print("El articulo de mayor precio es: ", lista[j])

def muestraArticulos(lista, lista2):
    valor=float(input("Ingrese precio: "))
    print("Productos menores al Precio ingresado: ")
    for i in range(len(lista2)):
        if lista2[i]<=valor:
            print(lista[i])

cargarDatos()
imprimePrecios(articulos, precios)
mayorPrecio(articulos, precios)
muestraArticulos(articulos, precios)