class Agenda:

    def __init__(self):
        self.contactos={}
    
    def menu(self):
        opcion=0
        self.datos=[]
        while opcion!=5:
            print("1 - Carga de un constacto en la agenda")
            print("2 - Listado completo de la agenda ")
            print("3 - Consulta ingresando el nombre de la persona")
            print("4 - Modificacion de su telefono, mail")
            print("5 - Finalizar programa")
            opcion=int(input("Elija una opcion por favor. \n "))

            if opcion==1:
                self.cargar()
            elif opcion==2:
                self.listar()
            elif opcion==3:
                self.consulta()
            elif opcion==4:
                self.modifica()

    def cargar (self):
        nombre=input("Introdusca nombre ")
        tel=int(input("Ingrese telefono "))
        mail=input("Ingrese mail ")
        #self.datos.append((tel, mail))
        self.contactos[nombre]=(tel, mail)
        print("______________________________________________")

    def listar(self):
        print("Listado de Contactos")
        for contacto in self.contactos:
            print("Nombre: ", contacto, self.contactos[contacto])
        print("______________________________________________")

    def consulta(self):
        nom=input("Ingrese nombre por favor. \n ")
        if nom in self.contactos:
            print(nom, self.contactos[nom][0], self.contactos[nom][1])
        else:
            print("No existe contacto!! ")
        print("______________________________________________")

    def modifica(self):
        nom=input("Ingrese nombre por favor. \n ")
        if nom in self.contactos:
            telefo=int(input("Ingrese telefono "))
            mail=input("Ingrese mail ")
            self.contactos[nom]=(telefo,mail)
        else:
            print("No existe contacto!! ")
        print("______________________________________________")


agenda=Agenda()
agenda.menu()