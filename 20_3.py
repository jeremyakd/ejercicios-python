"""
Crear una lista por asignación con la cantidad de elementos de tipo lista que usted desee.
Luego imprimir el último elemento de la lista principal.
"""

lista=[[1], [1,2], [1,2,3], [1,2,3,4], [1,2,3,4,5]]

#print(lista[len(lista-1)])
print(lista[(len(lista)-1)])