class Cuenta:

    def __init__(self, nombreTitular, monto):
        self.nombreTitular=nombreTitular
        self.monto=monto

    def imprime(self):
        print("Nombre: ", self.nombreTitular)
        print("Monto: ", self.monto)

class CajaAhorro(Cuenta):
    
    def __init__(self, nombreTitular, monto):
        super().__init__(nombreTitular, monto)
        
    def imprime(self):
        super().imprime()
    

class PlazoFijo(Cuenta):

    def __init__(self,titular,monto,plazo,interes):
        super().__init__(titular,monto)
        self.plazo=plazo
        self.interes=interes

    def imprime(self):
        print("Cuenta de plazo fijo")
        super().imprime()
        print("Plazo en dias:",self.plazo)
        print("Interes:",self.interes)
        self.ganancia_interes()

    def ganancia_interes(self):
        ganancia=self.monto*self.interes/100
        print("Importe del interes:",ganancia)


cajaahorro=CajaAhorro("Juan", 2000)
cajaahorro.imprime()

plazoFijo=PlazoFijo("Diego", 10000, 30, 0.75)
plazoFijo.imprime()