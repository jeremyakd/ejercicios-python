# A) - La primera letra de cada palabra.

""" def primeraLetra(cadena):
    cadena2=""
    cadena2+=cadena[0]

    for i in range(len(cadena)):
        if cadena[i]==" ":
            cadena2+=cadena[i+1]

    print(cadena2)

primeraLetra("Racing Club De Avellaneda") """

# B) -Cadena con la primera letra en mayuscula.

""" def primeraMayu(cadena):
    cadena2=""
    aux=""
    j=0
    for i in range(len(cadena)):
        if cadena[i]!=" " or i==(len(cadena)-1):
            aux+=cadena[i]
        else:
            cadena2+=aux.capitalize()
            cadena2+=cadena[i]
            i+=1
            aux=""
    cadena2+=aux.capitalize()    
    print(cadena2)

primeraMayu("racing club de avellaneda") """


# C) - Las palabras que comiencen con la letra A


def palabraConA(cadena):
    cadena2=""
    aux=""

    for i in range(len(cadena)):

        if cadena[i]!=" " or i==(len(cadena)-1):
            aux+=cadena[i]
        elif aux[0]=="A" or aux[0]=="a":
            cadena2+=aux
            cadena2+=cadena[i]
            #i+=1
            aux=""
        else:
            aux=""
    if aux[0]=="A" or aux[0]=="a":
            cadena2+=aux
                
    print(cadena2)

palabraConA("al be ad he ad el ar")