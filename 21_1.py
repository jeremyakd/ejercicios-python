"""
Se desea saber la temperatura media trimestral de cuatro paises. 
Para ello se tiene como dato las temperaturas medias mensuales de dichos paises.
Se debe ingresar el nombre del país y seguidamente las tres temperaturas medias mensuales.
Seleccionar las estructuras de datos adecuadas para el almacenamiento de los datos en memoria.
a - Cargar por teclado los nombres de los paises y las temperaturas medias mensuales.
b - Imprimir los nombres de las paises y las temperaturas medias mensuales de las mismas.
c - Calcular la temperatura media trimestral de cada país.
c - Imprimr los nombres de los paises y las temperaturas medias trimestrales.
b - Imprimir el nombre del pais con la temperatura media trimestral mayor.
"""

paises=[]
temperaturas=[]
tempmediatri=[]
suma=0

for i in range(4):
    pais=input("ingrese pais por favor: ")
    paises.append(pais)
    temp1=int(input("Ingrese temp media 1: "))
    temp2=int(input("Ingrese temp media 2: "))
    temp3=int(input("Ingrese temp media 3: "))
    temperaturas.append([temp1, temp2, temp3])

for i in range(4):
    suma=0
    for j in range(3):
        suma+=temperaturas[i][j]
    prom=suma/3
    tempmediatri.append(prom)
for i in range(4):
    print("Pais: " + paises[i], "Temp media: " + str(tempmediatri[i]))

mayor=tempmediatri[0]

x=0
for i in range(1,4):
    if tempmediatri[i]>mayor:
        mayor=tempmediatri[i]
        x=i

print("Pais con mayor temp. " + str(paises[x]))

