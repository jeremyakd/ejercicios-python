"""
Crear y cargar dos listas con los nombres de 5 productos en una y sus respectivos precios en otra.
Definir dos listas paralelas. Mostrar cuantos productos tienen un precio mayor al primer producto ingresado.
"""

productos=[]
precios=[]

for i in range(5):
    prod=input("Ingrese producto: ")
    productos.append(prod)
    pre=int(input("Ingrese precio: "))
    precios.append(pre)

igu=precios[0]
cantigu=0
for i in range(1,5):
    if precios[i]==igu:
        cantigu+=1
if cantigu>0:
    print("Cantidad de precios iguales al primero: " + str(cantigu) )
