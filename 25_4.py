"""
Plantear una función que reciba un string en mayúsculas o minúsculas
y retorne la cantidad de letras 'a' o 'A'.
"""

def contador(cadena):
    cant=0
    for i in cadena:
        if i=="a" or i=="A":
            cant+=1
    return cant

print(contador("abracadabra"))