"""
Almacenar en una lista de 5 elementos los nombres de empleados de una empresa 
junto a sus últimos tres sueldos (estos tres valores en una tupla)

El programa debe tener las siguientes funciones:

1)Carga de los nombres de empleados y sus últimos tres sueldos.
2)Imprimir el monto total cobrado por cada empleado.
3)Imprimir los nombres de empleados que tuvieron un ingreso trimestral mayor a 10000 
en los últimos tres meses.
"""

def cargaEmpleado():
    empl=[]
    for i in range(5):
        nom=input("Ingrese nombre del empleado. ")
        sue1=int(input("Ingrese Primer Sueldo: "))
        sue2=int(input("Ingrese Segundo Sueldo: "))
        sue3=int(input("Ingrese Tercer Sueldo: "))
        empl.append([nom,(sue1, sue2, sue3)])
    return empl

def total(lista):
    for i in range(5):
        sum=lista[i][1][0] + lista[i][1][1] + lista[i][1][2]
        print("Empleado: ", lista[i][0], "acum: ", sum)

def gananciasmayor(lista):
    for i in range(5):
        sum=lista[i][1][0] + lista[i][1][1] + lista[i][1][2]
        if sum>10000:
            print(lista[i][0], sum)


em=cargaEmpleado()
print(em)
total(em)
gananciasmayor(em)