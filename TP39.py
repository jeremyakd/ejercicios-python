# A) Inserte el caracter entre cada letra de la cadena.

""" def insertaCaracter():
    cadena=input("Ingrese cadena por favor. \n")
    caracter=input("Ingrese caracter por favor. \n")
    cant=int(input("Cuantas inserciones desea realizar \n"))
    cadena2=""
    f=0 # contador de la longitud de la cadena
    c=0 #contador de las inserciones
    for i in cadena:
        cadena2+=i
        f+=1
        if f<len(cadena) and c<cant:
            cadena2+=caracter
            c+=1
    print(cadena2)

insertaCaracter() """

# B) Reemplace todo los espacios por el caracter.

""" def reemplazaPorCaracter():
    cadena=input("Ingrese cadena por favor. \n")
    caracter=input("Ingrese caracter. \n")
    cant=int(input("Ingrese cantidad de reemplazos. \n"))
    cadena2=""
    c=0 #contador de las inserciones
    for i in range(len(cadena)):
        if cadena[i]==" " and c<cant:
            cadena2+=caracter
            c+=1
        else:
            cadena2+=cadena[i]
    print(cadena2)

reemplazaPorCaracter() """

# C) - Reemplace todos los digitos en la cadena por el caracter.

""" def remplazaDigito():
    cadena=input("Ingrese cadena por favor. \n")
    caracter=input("Ingrese caracter. \n")
    cant=int(input("Ingrese cantidad de reemplazos. \n"))
    cadena2=""
    c=0 #contador de las inserciones
    for i in cadena:
        if (i.isdigit()) and c<cant:
            cadena2+=caracter
            c+=1
        else:
            cadena2+=i
    return cadena2

print(remplazaDigito()) """


# D) - Inserte el caracter cada 3 digitos en la cadena

def inserteCaracterEnCadena():

    cadena=input("Ingrese cadena por favor. \n")
    caracter=input("Ingrese caracter. \n")
    cant=int(input("Ingrese cantidad de reemplazos. \n"))

    cadena2=""
    c=0
    for i in range(len(cadena)):
        if i>0:
            if i%3==0 and c<cant:
                cadena2+=caracter
                c+=1
        cadena2+=cadena[i]
        
    print(cadena2)

inserteCaracterEnCadena()