"""
Elaborar una función que nos retorne el perímetro de un cuadrado
pasando como parámetros el valor de un lado.
"""
def perimetro(lado):
    peri=lado*4
    return peri

print(perimetro(4))