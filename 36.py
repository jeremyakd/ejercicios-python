"""
Crear un diccionario en Python para almacenar los datos de empleados de una empresa. 
La clave será su número de legajo y en su valor 
almacenar una lista con el nombre, profesión y sueldo.
Desarrollar las siguientes funciones:

1) Carga de datos de empleados.
2) Permitir modificar el sueldo de un empleado. 
Ingresamos su número de legajo para buscarlo.
3) Mostrar todos los datos de empleados que tienen una profesión de
 "analista de sistemas"
"""

def cargaEmpleados():
    empleados={}
    continua="si"
    while continua=="si":
        leg=input("Ingrese numero de legajo ")
        nombre=input("Ingrese nombre ")
        profesion=input("Ingrese profesion ")
        sueldo=int(input("Ingrese sueldo "))
        empleados[leg]=[nombre,profesion,sueldo]
        continua=input("Desea cargar mas empleados  ")
    return empleados

def modificaSueldo(empleados):
    num=int(input("Ingrese numero de legajo  "))
    if num in empleados:
        sueldo=int(input("Ingrese sueldo a moficar: "))
        empleados[num][2]=sueldo
    else:
        print("No existe el empleado")

def analista(empleados):
    ana="analista de sistemas"
    for ana in empleados:
        print(empleados[ana])    

empleados=cargaEmpleados()
modificaSueldo(empleados)
#analista(empleados)
