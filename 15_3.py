"""
Una empresa tiene dos turnos (mañana y tarde) en los que trabajan 8 empleados
(4 por la mañana y 4 por la tarde)
Confeccionar un programa que permita almacenar los sueldos de los empleados agrupados en dos listas.
Imprimir las dos listas de sueldos.
"""

sueldosMan=[]
sueldosTar=[]

for i in range(4):
    su=float(input("Ingrese los sueldos del turno manana por favor:  "))
    sueldosMan.append(su)

for j in range(4):
    su=float(input("Ingrese los sueldos del turno tarde por favor:  "))
    sueldosTar.append(su)

print(sueldosMan)
print(sueldosTar)